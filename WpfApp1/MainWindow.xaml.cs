﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSearchClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(WordSearchRoutine);         
        }

        private void WordSearchRoutine(object state)
        {
            string url = "";
            Dispatcher.Invoke(() => { url = textBoxURL.Text; });
            
            HttpWebRequest request = WebRequest.CreateHttp(new Uri(url));
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader httpPage = new StreamReader(response.GetResponseStream());
            string pageTemp = httpPage.ReadToEnd();
            //textBoxLog.AppendText(pageTemp);
            string searchWord = ""; ;
            Dispatcher.Invoke(() => { searchWord = textBoxSearchWord.Text; });
            Dictionary<string, int> vocabulary = new Dictionary<string, int>();

            foreach (string word in pageTemp.Split(new char[] { ' ', ',', '.' }, StringSplitOptions.RemoveEmptyEntries))
            {

                if (vocabulary.ContainsKey(word))
                {
                    vocabulary[word]++;
                }
                else
                {
                    vocabulary.Add(word, 1);
                }

            }

            foreach (KeyValuePair<string, int> pair in vocabulary)
            {
               
                if (pair.Key == searchWord) Dispatcher.Invoke(() => { textBoxLog.AppendText($" word = {pair.Key} ; count =  {pair.Value} \n"); });

            }
        }
    }
}
